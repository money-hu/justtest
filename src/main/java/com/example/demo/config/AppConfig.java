package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/*
* 全局配置类
* */
@Configuration
public class AppConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //定义拦截规则
        registry.addInterceptor(new com.example.demo.config.LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/img/**")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/disk_login.html")
                .excludePathPatterns("/disk_list.html")
                .excludePathPatterns("/disk_reg.html")
                .excludePathPatterns("/user/reg")
                .excludePathPatterns("/user/login");
    }
}
