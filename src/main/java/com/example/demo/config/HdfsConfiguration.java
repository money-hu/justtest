package com.example.demo.config;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


import java.io.IOException;

@Component
public class HdfsConfiguration {
    @Value("${hadoop.fs.defaultFS}")
    private String defaultFS;

    @Bean
    public FileSystem fileSystem() throws IOException {
        Configuration config = new Configuration();
        config.set("fs.defaultFS", defaultFS);
        return FileSystem.get(config);
    }
}
