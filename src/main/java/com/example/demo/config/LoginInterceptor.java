package com.example.demo.config;

import com.example.demo.common.AppVariable;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/*
* 定义拦截器类
* */
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession httpSession = request.getSession(false);
        if(httpSession != null && httpSession.getAttribute(AppVariable.USER_SESSION_KEY) != null){
            //如果session存在，说明已经登录
            return true;
        }
        //未登录 重定向到登录页
        response.sendRedirect("/disk_list.html");
        return false;
    }
}
